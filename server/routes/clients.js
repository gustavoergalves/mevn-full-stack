const express = require('express');
const router = express.Router();
const API = require('../controllers/clients');

/**
 * @swagger
 * components:
 *   schemas:
 *     Client:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - phone
 *       properties:
 *         _id:
 *           type: string
 *           description: The id of the client
 *         name:
 *           type: string
 *           description: The name of the client
 *         email:
 *           type: email
 *           description: The email of the client
 *         phone:
 *           type: string
 *           description: The phone of the client
 *         providers:
 *           type: array
 *           description: The array of providers ids of the client
 *           items:
 *             type: object
 *             properties:
 *               _id:
 *                 type: string
 *       example:
 *         _id: 6137b674c6f025125d031700
 *         name: Gustavo
 *         email: gustavoergalves@gmail.com
 *         phone: +55 (35) 99747-4959
 *         providers: [
 *           {
 *               _id: 6137ad3fb3ad4acc8541b8e3
 *           }
 *         ]
 *     Provider:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *           description: The id of the provider
 *         name:
 *           type: string
 *           description: The name of the provider
 *       example:
 *         _id: 6137ad3fb3ad4acc8541b8e3
 *         name: Provider 1
 *     ClientsWithProviders:
 *       type: object
 *       properties:
 *         clients:
 *           type: array
 *           description: The array of clients
 *           items:
 *             $ref: '#/components/schemas/Client'
 *         providers:
 *           type: array
 *           description: The array of providers
 *           items:
 *             $ref: '#/components/schemas/Provider'
 */

/**
 * @swagger
 * tags:
 *   name: Clients
 *   description: The clients managing API
 */

/**
 * @swagger
 * /clients:
 *   get:
 *     summary: Returns the list of all the clients and providers
 *     tags: [Clients]
 *     responses:
 *       200:
 *         description: The list of the clients and providers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/ClientsWithProviders'
 *
 */
router.get("/", API.fetchAllClients);

/**
 * @swagger
 * /clients:
 *   post:
 *     summary: Create a new client
 *     tags: [Clients]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Client'
 *     responses:
 *       200:
 *         description: The client was successfully created
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/Client'
 *       500:
 *         description: Some server error
 *
 */
router.post("/", API.createClient);

/**
 * @swagger
 * /clients/{id}:
 *   patch:
 *     summary: Update a client by id
 *     tags: [Clients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The client id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Client'
 *     responses:
 *       200:
 *         description: The client was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/Client'
 *       404:
 *         description: The client was not found
 *       500:
 *         description: Some server error
 *
 */
router.patch("/:id", API.updateClient);

/**
 * @swagger
 * /clients/{id}:
 *   delete:
 *     summary: Delete a client by id
 *     tags: [Clients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The client id
 *     responses:
 *       204:
 *         description: The client was successfully deleted
 *       404:
 *         description: The client was not found
 *
 */
router.delete("/:id", API.deleteClient);

module.exports = router;
