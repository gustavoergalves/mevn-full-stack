const express = require('express');
const router = express.Router();
const API = require('../controllers/providers');

/**
 * @swagger
 * components:
 *   schemas:
 *     Provider:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *           description: The id of the provider
 *         name:
 *           type: string
 *           description: The name of the provider
 *       example:
 *         _id: 6137ad3fb3ad4acc8541b8e3
 *         name: Provider 1
 */

/**
 * @swagger
 * tags:
 *   name: Providers
 *   description: The providers managing API
 */


/**
 * @swagger
 * /providers:
 *   get:
 *     summary: Returns the list of all the providers
 *     tags: [Providers]
 *     responses:
 *       200:
 *         description: The list of the providers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Provider'
 *
 */
router.get("/", API.fetchAllProviders);

/**
 * @swagger
 * /providers:
 *   post:
 *     summary: Create a new provider
 *     tags: [Providers]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Provider'
 *     responses:
 *       200:
 *         description: The provider was successfully created
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/Provider'
 *       500:
 *         description: Some server error
 *
 */
router.post("/", API.createProvider);

/**
 * @swagger
 * /providers/{id}:
 *   patch:
 *     summary: Update a providers by id
 *     tags: [Providers]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The provider id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Provider'
 *     responses:
 *       200:
 *         description: The provider was successfully updated
 *         content:
 *           application/json:
 *             schema:
 *                 $ref: '#/components/schemas/Provider'
 *       404:
 *         description: The provider was not found
 *       500:
 *         description: Some server error
 *
 */
router.patch("/:id", API.updateProvider);

/**
 * @swagger
 * /providers/{id}:
 *   delete:
 *     summary: Delete a provider by id
 *     tags: [Providers]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The provider id
 *     responses:
 *       204:
 *         description: The provider was successfully deleted
 *       404:
 *         description: The provider was not found
 *
 */
router.delete("/:id", API.deleteProvider);

module.exports = router;
