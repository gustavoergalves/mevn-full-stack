# server

## Project setup
```
npm install
```
Create a mongodb database called `mevn_full_stack`

Create `.env` file.

Example:
```
SERVER_URI=http://localhost
SERVER_PORT=5000
DB_URI=mongodb://localhost:27017/mevn_full_stack
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Swagger Docs

You can find the API Docs at `{SERVER_HOST:SERVER_PORT}/api-docs`

Example: http://localhost:5000/api-docs
