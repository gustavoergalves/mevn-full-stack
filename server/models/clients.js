const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    name: {type: String},
    email: {type: String},
    phone: {type: String},
    providers: [
        {
            _id: {type: mongoose.Schema.Types.ObjectId, ref: 'Provider'}
        }
    ],
});

module.exports = mongoose.model('Client', clientSchema);
