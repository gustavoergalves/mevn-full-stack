const mongoose = require('mongoose');

const providerSchema = mongoose.Schema({
    name: {type: String},
});

module.exports = mongoose.model('Provider', providerSchema);
