const Client = require('../models/clients');
const Provider = require('../models/providers');

module.exports = class API {
    //fetch all clients
    static async fetchAllClients(req, res) {
        try {
            const clients = await Client.find();
            const providers = await Provider.find();
            const response = {
                clients: clients,
                providers: providers,
            };
            res.status(200).json(response);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //create new client
    static async createClient(req, res) {
        try {
            const client = req.body;
            const result = await Client.create(client);
            const insertedClient = await Client.findById(result._id);
            res.status(200).json(insertedClient);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //update client by id
    static async updateClient(req, res) {
        try {
            const id = req.params.id;
            const newClient = req.body;
            await Client.findByIdAndUpdate(id, newClient);
            const updatedClient = await Client.findById(id);
            res.status(200).json(updatedClient);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //delete client by id
    static async deleteClient(req, res) {
        try {
            const id = req.params.id;
            const result = await Client.findByIdAndDelete(id);
            if (!result) {
                res.status(404).json({message: 'The client was not found'});
            }
            res.status(200).json({message: 'Client deleted successfully'});
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }
}
