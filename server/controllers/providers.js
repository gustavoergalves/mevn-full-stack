const Provider = require('../models/providers');
const Client = require('../models/clients');

module.exports = class API {
    //fetch all providers
    static async fetchAllProviders(req, res) {
        try {
            const providers = await Provider.find();
            res.status(200).json(providers);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //create new provider
    static async createProvider(req, res) {
        try {
            const provider = req.body;
            const result = await Provider.create(provider);
            const insertedProvider = await Provider.findById(result._id);
            res.status(200).json(insertedProvider);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //update provider by id
    static async updateProvider(req, res) {
        try {
            const id = req.params.id;
            const newProvider = req.body;
            await Provider.findByIdAndUpdate(id, newProvider);
            const updatedProvider = await Provider.findById(id);
            res.status(200).json(updatedProvider);
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }

    //delete provider by id
    static async deleteProvider(req, res) {
        try {
            const id = req.params.id;

            const result = await Provider.findById(id);
            if (!result) {
                return res.status(404).json({message: 'The provider was not found'});
            }

            Client.findOne({'providers': {$elemMatch: {_id: id}}}, async function (err, client) {
                if (err) {
                    return res.status(500).json({message: err.message});
                }

                if (client) {
                    return res.status(400).json({message: 'The provider cannot be deleted'});
                } else {
                    const result = await Provider.findByIdAndDelete(id);
                    if (!result) {
                        return res.status(404).json({message: 'The provider was not found'});
                    }
                    return res.status(200).json({message: 'Provider deleted successfully'});
                }
            });
        } catch (err) {
            res.status(500).json({message: err.message});
        }
    }
}
