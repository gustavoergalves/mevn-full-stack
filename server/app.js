//imports
require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const swaggerUI = require('swagger-ui-express');
const swaggerJSDocs = require('swagger-jsdoc');

const server = process.env.SERVER_URI || 'http://localhost';
const port = process.env.SERVER_PORT || 5000;

const options = {
    apis: ['./routes/*.js'],
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Clients API',
            version: '1.0.0',
            description: 'Clients API'
        },
        servers: [
            {
                url: `${server}:${port}`
            }
        ]
    }
};

const specs = swaggerJSDocs(options);

const app = express();


//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//routes prefix
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs));
app.use('/clients', require('./routes/clients'));
app.use('/providers', require('./routes/providers'));

//db conn
mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true
})
    .then(() => console.log('conn ok'))
    .catch((err) => console.log(err));

//start server
app.listen(port, () => {
    console.log('Server is running! Port: ' + port);
});
