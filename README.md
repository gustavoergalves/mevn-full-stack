# mevn_full_stack

## Getting Started

The project is divide into two parts: client and server.

The project was developed using the MongoDB, Express.js, VueJS, Node.js (MEVN stack).

Inside the client folder, there is the Vue.js project called clients-app. There, you will find its README file which will give the directions of how to install, run and build the project.

Inside the server folder, there is the Node.js project and also its README file with the all instructions needed.

**Obs**: Remember to get into the path of the chosen project to run the commands.
