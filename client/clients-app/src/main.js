import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueResource from 'vue-resource'
import { VueMaskDirective } from 'v-mask'

Vue.directive('mask', VueMaskDirective);

Vue.config.productionTip = false

Vue.use(VueResource)
Vue.http.options.root = process.env.VUE_APP_API_BASE_URL

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
