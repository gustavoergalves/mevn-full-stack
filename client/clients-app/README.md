# clients-app

## Project setup
```
npm install
```

Create `.env` file

Example:
```
VUE_APP_API_BASE_URL=http://localhost:5000
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
